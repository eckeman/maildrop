# Redis subnet group

resource "aws_elasticache_subnet_group" "redis" {
  name = "redissubnet"
  subnet_ids = ["${aws_subnet.public.id}"]
}

# Redis Elasticache instance

resource "aws_elasticache_cluster" "redis" {
  cluster_id = "maildrop-cluster"
  engine = "redis"
  engine_version = "5.0.3"
  node_type = "cache.t2.small"
  num_cache_nodes = 1
  port = "6379"
  subnet_group_name = "${aws_elasticache_subnet_group.redis.name}"
  parameter_group_name = "default.redis5.0"
  security_group_ids = ["${aws_security_group.redis.id}"]
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
}

output "redis" {
  value = "${aws_elasticache_cluster.redis.cache_nodes.0.address}:${aws_elasticache_cluster.redis.port}"
}
